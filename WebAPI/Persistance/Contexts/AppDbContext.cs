using Microsoft.EntityFrameworkCore;
using WebAPI.Domain.Models;
using System.Collections.Generic;

namespace WebAPI.Persistance.Contexts 
{
    public class AppDbContext : DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Author> Authors { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            modelBuilder.Entity<Post>()
                .HasOne(b => b.Author)
                .WithMany(a => a.Posts)
                .HasForeignKey(fk => fk.AuthorId);
        }
    }
}