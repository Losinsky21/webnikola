
using WebAPI.Domain.Repositories;
using WebAPI.Persistance.Contexts;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Persistance.Repositories
{
    public class PostRepository : BaseRepository, IPostRepository
    {

        public PostRepository(AppDbContext appDbContext) : base (appDbContext) {}

        public async Task AddPostAsync(Post post)
        {
            await _appDbContext.Posts.AddAsync(post);
        }

        public async Task<IEnumerable<Post>> ListSync()
        {
            return await _appDbContext.Posts.ToListAsync();
        }
    }
}