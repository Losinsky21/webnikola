using Microsoft.EntityFrameworkCore;
using WebAPI.Domain.Repositories;
using WebAPI.Persistance.Contexts;
using System.Collections.Generic;
using WebAPI.Domain.Models;
using System.Threading.Tasks;

namespace WebAPI.Persistance.Repositories
{
    public class AuthorRepository : BaseRepository, IAuthorRepository
    {
        public AuthorRepository(AppDbContext appDbContext) : base(appDbContext) { }

        public async Task AddAuthorAsync(Author author)
        {
            await _appDbContext.Authors.AddAsync(author);
        }

        public async Task<IEnumerable<Author>> ListAsync()
        {
            return await  _appDbContext.Authors.ToListAsync();
        }
    }
}