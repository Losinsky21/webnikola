using WebAPI.Persistance.Contexts;

namespace WebAPI.Persistance.Repositories
{
    public abstract class BaseRepository 
    {
        protected readonly AppDbContext _appDbContext;

        public BaseRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }
        
    }
}