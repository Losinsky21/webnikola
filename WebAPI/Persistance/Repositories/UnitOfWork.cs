using System.Threading.Tasks;
using WebAPI.Domain.Repositories;
using WebAPI.Persistance.Contexts;

namespace WebAPI.Persistance.Repositories 
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        public UnitOfWork(AppDbContext appDbContext) 
        {
            _appDbContext = appDbContext;
        }
        public async Task CompleteSaveAsync()
        {
            await _appDbContext.SaveChangesAsync();
        }
    }
}