using Microsoft.AspNetCore.Mvc;
using WebAPI.Domain.Services;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Resources;
using WebAPI.Domain.Models;
using AutoMapper;
using WebAPI.Extensions;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthorController : Controller
    {
        private readonly IAuthorService _authorService;
        private readonly IMapper _mapper;

        public AuthorController(IAuthorService authorService, IMapper mapper)
        {
            _authorService = authorService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<AuthorResource>> GetAllAuthors()
        {
            var authors = await _authorService.ListAsync();
            var authorResources = _mapper.Map<IEnumerable<Author>, IEnumerable<AuthorResource>>(authors);
            return authorResources;
        }

        [HttpPost]
        public async Task<IActionResult> AddNewAuthor([FromBody] SaveAuthorResource saveAuthorResource)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var author = _mapper.Map<SaveAuthorResource, Author>(saveAuthorResource);
            var authorResult = await _authorService.SaveAuthorAsync(author);

            if (!authorResult.Success)
            {
                return BadRequest(authorResult.Message);
            }

            var authorResource = _mapper.Map<Author, AuthorResource>(authorResult.Author);
            return Ok(authorResource);
        }
    }
}