using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using WebAPI.Domain.Models;
using WebAPI.Domain.Services;
using WebAPI.Resources;
using System.Threading.Tasks;
using AutoMapper;
using WebAPI.Extensions;
using Microsoft.EntityFrameworkCore;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]

    public class PostController : ControllerBase
    {
        private readonly IPostService _postService;
        private readonly IMapper _mapper;

        public PostController(IPostService postService, IMapper mapper)
        {
            _postService = postService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IEnumerable<PostResource>> GetAllPosts()
        {
            var posts = await _postService.ListSync();
            var postResource = _mapper.Map<IEnumerable<Post>, IEnumerable<PostResource>>(posts);
            return postResource;
        }

        [HttpPost]
        public async Task<IActionResult> AddNewPost([FromBody] SavePostResource savePostResource)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState.GetErrorMessages());
            }

            var post = _mapper.Map<SavePostResource, Post>(savePostResource);
            var postResult = await _postService.SavePostAsync(post);

            if (!postResult.Success)
            {
                return BadRequest(postResult.Message);
            }

            var postResource = _mapper.Map<Post, PostResource>(postResult.Post);
            return Ok(postResource);
        }
    }
}