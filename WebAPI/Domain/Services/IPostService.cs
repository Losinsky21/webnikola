using WebAPI.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Domain.Services.Communication;

namespace WebAPI.Domain.Services
{
    public interface IPostService
    {
        Task<IEnumerable<Post>> ListSync();
        Task<SavePostResponse> SavePostAsync(Post post);
    }
}