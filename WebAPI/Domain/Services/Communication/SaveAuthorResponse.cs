using WebAPI.Domain.Models;

namespace WebAPI.Domain.Services.Communication
{
    public class SaveAuthorResponse : BaseResponse
    {
        public Author Author; 
        private SaveAuthorResponse(bool success, string message, Author author) : base (success, message)
        {
            Author = author;
        }

        public SaveAuthorResponse(Author author) : this(true, string.Empty, author) {}

        public SaveAuthorResponse(string message): this(false, message, null) {}
    }
}