using WebAPI.Domain.Models;


namespace WebAPI.Domain.Services.Communication
{
    public class SavePostResponse : BaseResponse
    {
        public Post Post;
        private SavePostResponse(bool success, string message, Post post) : base (success, message)
        {
            Post = post;
        }

        public SavePostResponse(Post post) : this(true, string.Empty, post)
        { }

        public SavePostResponse(string message): this(false, message, null)
        { }
    }
}