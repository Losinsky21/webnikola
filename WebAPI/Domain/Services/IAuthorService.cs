using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Domain.Models;
using WebAPI.Domain.Services.Communication;
using WebAPI.Resources;

namespace WebAPI.Domain.Services
{
    public interface IAuthorService 
    {
        Task<IEnumerable<Author>> ListAsync();
        Task<SaveAuthorResponse> SaveAuthorAsync(Author author);
    }
}