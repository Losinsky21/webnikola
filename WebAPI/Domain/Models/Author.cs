using System;
using System.Collections.Generic;

namespace WebAPI.Domain.Models
{
    public class Author
    {
        public int AuthorId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public virtual ICollection<Post> Posts { get; set;}
    }
}