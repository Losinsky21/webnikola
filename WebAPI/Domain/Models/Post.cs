using System;
using System.Collections.Generic;

namespace WebAPI.Domain.Models
{
    public class Post 
    {
        public int PostId { get; set; }
        public string Content { get; set; }
        public DateTime Data { get; set; }

        public virtual Author Author { get; set; }
        public int AuthorId { get; set; }
        
    }
}