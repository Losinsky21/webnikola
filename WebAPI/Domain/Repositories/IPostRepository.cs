using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Domain.Models;
using WebAPI.Domain.Services.Communication;

namespace WebAPI.Domain.Repositories 
{
    public interface IPostRepository
    {
        Task<IEnumerable<Post>> ListSync();
        Task AddPostAsync(Post post);
    }
}