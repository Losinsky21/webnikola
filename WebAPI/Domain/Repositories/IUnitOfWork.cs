using System.Threading.Tasks;

namespace WebAPI.Domain.Repositories 
{
    public interface IUnitOfWork
    {
        Task CompleteSaveAsync();
    }
}