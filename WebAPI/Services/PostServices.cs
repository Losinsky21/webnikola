using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Domain.Services;
using WebAPI.Domain.Models;
using WebAPI.Domain.Repositories;
using WebAPI.Domain.Services.Communication;
using System;

namespace WebAPI.Services 
{
    public class PostService : IPostService {

        private readonly IPostRepository _postRepository;
        private readonly IUnitOfWork _unitOfWork;

        public PostService(IPostRepository postRepository, IUnitOfWork unitOfWork)
        {
            _postRepository = postRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Post>> ListSync()
        {
            return await _postRepository.ListSync();
        }

        public async Task<SavePostResponse> SavePostAsync(Post post)
        {
           try
           {
               await _postRepository.AddPostAsync(post);
               await _unitOfWork.CompleteSaveAsync();

               return new SavePostResponse(post);
           }
           catch (Exception except)
           {
               return new SavePostResponse($"An error occured. Log: {except.Message}");
           }
        }
    }
}