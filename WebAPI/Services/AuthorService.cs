using WebAPI.Domain.Repositories;
using WebAPI.Domain.Services;
using WebAPI.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPI.Domain.Services.Communication;
using System;
using WebAPI.Resources;

namespace WebAPI.Services 
{
    public class AuthorService : IAuthorService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AuthorService(IAuthorRepository authorRepository, IUnitOfWork unitOfWork)
        {
            _authorRepository = authorRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Author>> ListAsync() 
        {
            return await _authorRepository.ListAsync();
        }

        public async Task<SaveAuthorResponse> SaveAuthorAsync(Author author)
        {
            try
            {
                await _authorRepository.AddAuthorAsync(author);
                await _unitOfWork.CompleteSaveAsync();

                return new SaveAuthorResponse(author);
            }
            catch (Exception exception)
            {
                return new SaveAuthorResponse($"An error occured. Log: {exception.Message}");
            }
        }
    }
}