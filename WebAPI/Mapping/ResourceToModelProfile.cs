using AutoMapper;
using WebAPI.Domain.Models;
using WebAPI.Resources;

namespace WebAPI.Mapping
{
    public class ResourceToModelProfile : Profile
    {
        public ResourceToModelProfile()
        {
            CreateMap<SavePostResource, Post>();
            CreateMap<SaveAuthorResource, Author>();
        }
    }
}