using AutoMapper;
using WebAPI.Domain.Models;
using WebAPI.Resources;

namespace WebAPI.Mapping 
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Post, PostResource>();
            CreateMap<Author, AuthorResource>();
        }
    }
}