using System;
using WebAPI.Domain.Models;
using System.Collections.Generic;

namespace WebAPI.Resources
{
    public class AuthorResource
    {
        // public int AuthorId { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        // public ICollection<Post> Posts { get; set;}
    }
}