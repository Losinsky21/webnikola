using System.ComponentModel.DataAnnotations;
using WebAPI.Domain.Models;
using System;

namespace WebAPI.Resources
{
    public class SaveAuthorResource
    {
        [Required]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }
    }
}