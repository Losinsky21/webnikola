using System.ComponentModel.DataAnnotations;
using WebAPI.Domain.Models;
using System;

namespace WebAPI.Resources
{    public class SavePostResource 
    {
        [Required]
        public int AuthorId { get; set; }

        [Required]
        [MaxLength(30)]
        public string Content { get; set; }

        [Required]
        public DateTime Data { get; set; }
    }
}