using System;
using WebAPI.Domain.Models;

namespace WebAPI.Resources
{
    public class PostResource 
    {
        public int PostId { get; set; }
        public string Content { get; set; }
        public DateTime Data { get; set; }
    }
}